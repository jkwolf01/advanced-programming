#pragma once 
#include <iostream>

template <class T, class G>
class my_pair{
	private:
		T first;
		G second;
	public:
		my_pair(T one, G two) { first = one; second = two; }
		T get_first() { return first; }
		G get_second() { return second; }
};