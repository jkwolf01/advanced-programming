#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <thread>
#include <iostream>
#include <mutex>
#include <map>
#include <vector>
#include "my_pair.h"

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 1024
#define DEFAULT_PORT "27015"

std::map<SOCKET, std::string> players_sockets;

void sign_up(int ClientSocket, std::vector<std::string> args){

}

void login(int ClientSocket, std::vector<std::string> args){

}

void logout(int ClientSocket){

}

void room_list(int ClientSocket){

}

void create_game(int ClientSocket, std::vector<std::string> args){

}

void join_game(int ClientSocket, std::vector<std::string> args){

}

void start_game(int ClientSocket){

}

void leave_game(int ClientSocket){

}

void close_game(int ClientSocket){

}

void move(int ClientSocket, std::vector<std::string> args){

}
void draw_cards(int ClientSocket, std::vector<std::string> args){

}

void make_order(int ClientSocket, int order, std::vector<std::string> args){
	char error[DEFAULT_BUFLEN] = "";
	switch (order){
		case 1:
			sign_up(ClientSocket, args);
			break;
		case 2:
			login(ClientSocket, args);
			break;
		case 3:
			logout(ClientSocket);
			break;
		case 10:
			room_list(ClientSocket);
			break;
		case 11:
			create_game(ClientSocket, args);
			break;
		case 12:
			join_game(ClientSocket, args);
			break;
		case 13:
			start_game(ClientSocket);
			break;
		case 14:
			leave_game(ClientSocket);
			break;
		case 15:
			close_game(ClientSocket);
			break;
		case 20:
			move(ClientSocket, args);
			break;
		case 21:
			draw_cards(ClientSocket, args);
			break;
		default:
			strcpy_s(error, DEFAULT_BUFLEN, "@130||");
			break;
	}
	if (strcmp(error, "") != 0){
		int iResult = send(ClientSocket, error, (int)strlen(error), 0);
		if (iResult == SOCKET_ERROR) {
			wprintf(L"send failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
		}
	}
}

void parse(int ClientSocket, char* msg){
	unsigned int i = 1;
	char num[2] = "";
	char error[DEFAULT_BUFLEN] = "";
	char arg[DEFAULT_BUFLEN] = "";
	std::vector<std::string> args;
	if (strlen(msg) < 4)
		strcpy_s(error, DEFAULT_BUFLEN, "@130||");
	if (msg[0] == '@' && strcmp(error, "") == 0){
		int j = 0;
		while (msg[i] != '|'){
			if (i == 3)
				strcpy_s(error, DEFAULT_BUFLEN, "@130||");
			num[j] = msg[i];
			j++;
			i++;
		}
		i++;
		int order = atoi(num);
		if (strcmp(error, "") == 0){
			j = 0;
			while (i < strlen(msg)){
				if (msg[i] != '|'){
					arg[j] = msg[i];
					j++;
				}
				else{
					arg[j] = '\0';
					args.push_back(arg);
					j = 0;
					strcpy_s(arg, DEFAULT_BUFLEN, "");
				}
				i++;
			}
			make_order(ClientSocket, order, args);
		}
	}
	if (strcmp(error, "") != 0){
		int iResult = send(ClientSocket, error, (int)strlen(error), 0);
		if (iResult == SOCKET_ERROR) {
			wprintf(L"send failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
		}
	}
}


void player(int ClientSocket){
	int iResult;
	char recvbuf[DEFAULT_BUFLEN] = "";
	int recvbuflen = DEFAULT_BUFLEN;
	do {
		iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0){
			parse(ClientSocket, recvbuf);
		}
		else if (iResult == 0)
			printf("Connection closed\n");
		else
			printf("recv failed: %d\n", WSAGetLastError());
		strcpy_s(recvbuf, recvbuflen, "");
	} while (iResult > 0);
}

int listen()
{
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}
	std::vector<std::thread*> players;
	/*we have connection*/
	while (ClientSocket = accept(ListenSocket, NULL, NULL)){
		if (ClientSocket == INVALID_SOCKET) {
			printf("accept failed with error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			WSACleanup();
			return 1;
		}
		std::thread *t1= new std::thread(player, ClientSocket);
		players.push_back(t1);
	}

	closesocket(ListenSocket);
	closesocket(ClientSocket);
	WSACleanup();

	return 0;
}

int main(){
	listen();
	return 0;
}