#include <thread>
#include <iostream>
#include <chrono>
#include <vector>
#include <mutex>
#include <fstream>
#include <condition_variable>

bool writer = false;/*is there a writer*/
bool empty = true;/*is file empty?*/
int readers = 0;/*readers count*/
bool closeAll = false;

std::condition_variable for_the_readers;/*reader  can access?*/
std::condition_variable for_the_writers;/*writer can access?*/

std::fstream f;/*file*/
std::mutex lock_vars;/*lock vars*/

std::mutex lock_cout;/*lock the cout*/

/*is the file empty?*/
bool file_empty(){
	return empty;
}

/*
check if reader can access the file
*/
bool access_read(){
	for_the_readers.notify_one();
	return (!writer && !file_empty());
}


/*
check if writer can access the file
*/
bool access_write(){
	for_the_writers.notify_one();
	return (!writer && (readers == 0 || file_empty()));
}

/*
read thread check if someone use the file
and if he can use the file then going to sleep for 2 sec
and the writing that he finished
*/
void read(){
	std::unique_lock<std::mutex> lck_cout(lock_cout, std::defer_lock);
	std::unique_lock<std::mutex> lck(lock_vars);
	readers++;
	while (!access_read()){
		for_the_readers.wait(lck);
		if (closeAll && !access_read()){
			if (lck.owns_lock())
				lck.unlock();
			exit(0);
		}
	}
	if (!lck.owns_lock())
		lck.lock();
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
	lck_cout.lock();
	std::cout << "Reading..." << std::endl;
	lck_cout.unlock();
	empty = true;
	readers--;
	for_the_readers.notify_one();
	for_the_writers.notify_one();
	lck.unlock();
}


/*
write thread check if someone use the file 
and if he can use the file then going to sleep for 2 sec
and the writing that he finished
*/
void write(){
	std::unique_lock<std::mutex> lck(lock_vars, std::defer_lock);
	std::unique_lock<std::mutex> lck_cout(lock_cout, std::defer_lock);
	while (!access_write()) for_the_writers.wait(lck);
	lck.lock();
	writer = true;
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
	lck_cout.lock();
	std::cout << "Writing..." << std::endl;
	lck_cout.unlock();
	empty = false;
	writer = false;
	for_the_readers.notify_one();
	lck.unlock();
}



/*
main opens file and making the menu for the user if he want to write or read
when the user exits the menu main thread join to all the threads that still running 
****bug****
if you exit the menu and there are still readers and the file still empty
the program will stuck!!!
****bug****
*/
int main(int argc, char** argv){
	f.open(argv[1]);  //no other threads yet, no need to lock it
	if (!f.is_open())
		exit(1);
	

	std::unique_lock<std::mutex> lck(lock_cout, std::defer_lock);
	std::vector<std::thread> threads;
	int choice = 0;
	std::cout << "1. Write" << std::endl;
	std::cout << "2. Read" << std::endl;
	std::cout << "3. Exit" << std::endl;
	std::cout << "Enter your choice: ";
	do{
		std::cin >> choice;
		switch (choice)
		{
		case 1:
			threads.push_back(std::thread(write));
			break;
		case 2:
			threads.push_back(std::thread(read));
			break;
		case 3:
			lck.lock();
			std::cout << "Good Bye!!!" << std::endl;
			lck.unlock();
			break;
		default:
			lck.lock();
			std::cout << "No such an option!!!" << std::endl;
			lck.unlock();
			break;
		}
	} while (choice != 3);
	lock_vars.lock();
	closeAll = true;
	for_the_readers.notify_all();
	lock_vars.unlock();
	for (unsigned int i = 0; i < threads.size(); i++)
		if (threads[i].joinable()) 
			threads[i].join();
	f.close();
	system("pause");
	return 0;
}